<div class="content-wrapper">
<div class="col-md-6">
<div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Input Barang Baru</h3>
              </div>
			  <div class="row">
			  <div class="col-12">
				<?php
				if(!empty($this->session->flashdata('pesan'))){
					?>
			  <div class="alert alert-warning alert-dismissible">

 				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-exclamation-triangle"></i> Alert!</h5>
                  
				  <?php echo $this->session->flashdata('pesan'); ?>
				  </div> 

				<?php
				}

				$id = isset($data->id) ? $data->id : null;
				$product_code = isset($data->product_code) ? $data->product_code : null;
				$product_name = isset($data->product_name) ? $data->product_name : null;
				$price = isset($data->price) ? $data->price : null;
				$currency = isset($data->currency) ? $data->currency : null;
				$discount = isset($data->discount) ? $data->discount : null;
				$dimension = isset($data->dimension) ? $data->dimension : null;
				$unit = isset($data->unit) ? $data->unit : null;

				?>
                 
			  

          </div>

			  </div>
              <div class="card-body">
			  <form action="<?php
			  if(isset($this->session->proses) && $this->session->proses == 'edit'){
				echo base_url('barang/update/'.$id);
			}else{
				echo base_url('barang/simpan');
			}
			   
			    ?>" method="post">
			  <div class="form-group row">
			  <label>Kode Barang</label>
                <input class="form-control" type="text" id="product_code" name="product_code" placeholder="" value="<?php echo $product_code ?>">
				</div>
				<div class="form-group row">
				<label>Nama Barang</label>
                <input class="form-control" type="text" id="product_name" name="product_name" placeholder="" value="<?php echo $product_name ?>">
				</div>
				<div class="form-group row">
				<label>Harga</label>
                <input class="form-control" type="text" id="price" name="price" placeholder="" value="<?php echo $price ?>">
				</div>
				<div class="form-group row">
				<label>Mata Uang</label>
                <input class="form-control" type="text" id="currency" name="currency" placeholder="" value="<?php echo $currency ?>">
				</div>
				<div class="form-group row">
				<label>Diskon</label>
                <input class="form-control" type="text" id="discount" name="discount" placeholder="" value="<?php echo $discount ?>">
				</div>
				<div class="form-group row">
				<label>Dimensi</label>
                <input class="form-control" type="text" id="dimension" name="dimension" placeholder="" value="<?php echo $dimension ?>">
				</div>
				<div class="form-group row">
				<label>Unit</label>
                <input class="form-control" type="text" id="Unit" name="unit" placeholder="" value="<?php echo $unit ?>">
				</div>
			  <div class="row">
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">
				<?php if(isset($this->session->proses) && $this->session->proses == 'edit'){
					echo "Update";
				}else{
					echo "Simpan";
				}
				?>
			</button>
          </div>
          <!-- /.col -->
        </div>
		</form>

              </div>
              <!-- /.card-body -->
            </div>
</div>
</div>
