<?php defined('BASEPATH') OR exit("No direct script access allowed");

$this->load->view("layout-top");
$this->load->view($view);
$this->load->view("layout-bot");
