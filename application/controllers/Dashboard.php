<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function index()
	{
		if(isset($this->session->nama) && $this->session->nama != null){
			$data['view'] = 'dashboard';
			$this->load->view('layout', $data);
		}else{
			redirect(base_url('login'));
		}
	}
}
