<?php
class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->library('form_validation');
        $this->load->helper('form');
    }

    public function index()
    {
        return $this->load->view('form_login');
    }

    public function cek_login()
    {
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');

        $json = (array) json_decode(file_get_contents('php://input'));

        if (count($json) <= 0 && $this->form_validation->run() == false) {
			$this->session->set_flashdata('pesan', '<br>Data masih kosong');
            $this->load->view('form_login');
        } else {
            $user_name = isset($json['username']) ? $json['username'] : $this->input->post('username');
            $password = isset($json['password']) ? $json['password'] : $this->input->post('password');

            $where = array(
                'user' => $user_name,
                'password' => md5($password),
            );

            $cek = $this->Login_model->authLogin("logins", $where)->num_rows();

            if ($cek > 0) {
                $data_session = array(
                    'nama' => $user_name,
                    'status' => "login",
                );

                $this->session->set_userdata($data_session);
                redirect(base_url("Dashboard"));
            } else {
                $this->session->set_flashdata('pesan', '<br>Username atau password salah');
                redirect(base_url("Login"));
            }
            // $this->session->set_userdata($data_session);
            // echo "Berhasil Login";
        }

    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url("Login"));
    }

}
