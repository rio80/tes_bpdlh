<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Barang_model','model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		if(isset($this->session->nama) && $this->session->nama != null){
			$data['data'] = $this->getAllBarang();
			$data['view'] = 'form_barang';
			$this->load->view('layout', $data);
		}else{
			redirect(base_url('login'));
		}
	}

	function getAllBarang(){
		$data = $this->model->getBarang();
		return $data;
	}

	function getDetailBarang($id){
		$data = $this->model->getBarang($id);
		return $data;
	}

	function new_barang(){
			$this->session->set_userdata(['proses' => 'baru']);
			$data['view'] = 'form_barang_input';
			$this->load->view('layout', $data);
	}

	function simpan(){
		$this->form_validation->set_rules('product_code', 'Kode Barang', 'trim|required');
        $this->form_validation->set_rules('product_name', 'Nama Barang', 'trim|required');
        $this->form_validation->set_rules('price', 'Harga', 'trim|required');
        $this->form_validation->set_rules('currency', 'Mata Uang', 'trim|required');
        $this->form_validation->set_rules('discount', 'Diskon', 'trim');
        $this->form_validation->set_rules('dimension', 'Dimensi', 'trim|required');
        $this->form_validation->set_rules('unit', 'Unit', 'trim|required');

		$data['view'] = 'form_barang_input';

		if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('pesan', '<br>Harap isikan data dengan benar');
			$this->load->view('layout', $data);
        }else{

			$data = [
				'product_code' => $this->input->post('product_code'),
				'product_name' => $this->input->post('product_name'),
				'price' => $this->input->post('price'),
				'currency' => $this->input->post('currency'),
				'discount' => $this->input->post('discount'),
				'dimension' => $this->input->post('dimension'),
				'unit' => $this->input->post('unit')
			];
	
	
			$cek = $this->model->saveBarang($data);
	
			if($cek){
				redirect(base_url('barang'));
			}else{
				$this->session->set_flashdata('pesan', '<br>Data belum benar, harap periksa');
				$this->load->view('layout', $data);
			}
		}
	}

	function edit($id){

		$this->session->set_userdata(['proses' => 'edit']);
		$data['data'] = $this->getDetailBarang(['id' => $id]);
		$data['view'] = 'form_barang_input';
		$this->load->view('layout', $data);

	}

	function update($id){
		$this->form_validation->set_rules('product_code', 'Kode Barang', 'trim|required');
        $this->form_validation->set_rules('product_name', 'Nama Barang', 'trim|required');
        $this->form_validation->set_rules('price', 'Harga', 'trim|required');
        $this->form_validation->set_rules('currency', 'Mata Uang', 'trim|required');
        $this->form_validation->set_rules('discount', 'Diskon', 'trim');
        $this->form_validation->set_rules('dimension', 'Dimensi', 'trim|required');
        $this->form_validation->set_rules('unit', 'Unit', 'trim|required');

		if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('pesan', '<br>Harap isikan data dengan benar');
			$this->load->view('layout', $data);
        }else{

			$data = [
				'product_code' => $this->input->post('product_code'),
				'product_name' => $this->input->post('product_name'),
				'price' => $this->input->post('price'),
				'currency' => $this->input->post('currency'),
				'discount' => $this->input->post('discount'),
				'dimension' => $this->input->post('dimension'),
				'unit' => $this->input->post('unit')
			];
	
	
			$cek = $this->model->update($data, ['id' => $id]);
	
			if($cek){
				redirect(base_url('barang'));
			}else{
				$this->session->set_flashdata('pesan', '<br>Data belum benar, harap periksa');
				$this->load->view('layout', $data);
			}
		}
	}

	function delete($id){
		$cek = $this->model->deleted(['id' => $id]);
		if($cek){
			redirect(base_url('barang'));
		}
	}
}
