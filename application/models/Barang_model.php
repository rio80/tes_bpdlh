<?php


class Barang_model extends CI_Model {
	public $table = 'products';

	function getBarang($where = null){
		$this->db->from($this->table);
		if($where != null){
			$this->db->where($where);
			return $this->db->get()->row();
		}
		return $this->db->get()->result();
	}

	function saveBarang($data){
		$this->db->insert($this->table, $data);
		return $this->db->affected_rows();
	}
	
	function update($data, $where){
		$this->db->where($where);
		$this->db->update($this->table, $data);
		return $this->db->affected_rows();
	}

	function deleted($where){
		$this->db->from($this->table);
		$this->db->where($where);
		$this->db->delete();
		return $this->db->affected_rows();
	}
}
