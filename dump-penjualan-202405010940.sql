-- MySQL dump 10.13  Distrib 5.7.24, for Win64 (x86_64)
--
-- Host: localhost    Database: penjualan
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `checkouts`
--

DROP TABLE IF EXISTS `checkouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `checkouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_code` varchar(18) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `currency` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtotal` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `checkouts`
--

LOCK TABLES `checkouts` WRITE;
/*!40000 ALTER TABLE `checkouts` DISABLE KEYS */;
/*!40000 ALTER TABLE `checkouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logins`
--

DROP TABLE IF EXISTS `logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logins`
--

LOCK TABLES `logins` WRITE;
/*!40000 ALTER TABLE `logins` DISABLE KEYS */;
INSERT INTO `logins` VALUES (1,'rio','202cb962ac59075b964b07152d234b70'),(2,'test','81dc9bdb52d04dc20036dbd8313ed055');
/*!40000 ALTER TABLE `logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2022_06_30_020954_create_logins_table',1),(5,'2022_06_30_021352_create_products_table',1),(6,'2022_06_30_021710_create_transaction_headers_table',1),(7,'2022_06_30_021946_create_transaction_details_table',1),(8,'2022_06_30_050712_create_checkouts_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_code` varchar(18) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `currency` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` int(11) NOT NULL,
  `dimension` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (2,'GIB','Giv Biru',15000,'Rp.',20000,'2 cm x 6 cm','pcs',NULL,NULL),(3,'SOL','SO Klin Liquid',29000,'Rp.',0,'13 cm x 10 cm','pcs',NULL,NULL),(8,'TV','Televisi Toshiba',2000000,'Rp',100000,'1x1','pcs',NULL,NULL),(10,'PGR','Pagar Besi',4000000,'Rp',100000,'3x3','unit',NULL,NULL),(13,'CHG','Charger Hp Samsung',20000,'Rp',10,'1x1','pcs',NULL,NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_details`
--

DROP TABLE IF EXISTS `transaction_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `document_code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document_number` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_code` varchar(18) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtotal` int(11) NOT NULL,
  `currency` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_details`
--

LOCK TABLES `transaction_details` WRITE;
/*!40000 ALTER TABLE `transaction_details` DISABLE KEYS */;
INSERT INTO `transaction_details` VALUES (4,'TRX','9','SOK',13500,1,'pcs',0,'Rp. ','2022-06-29 22:29:43','2022-06-29 22:29:43'),(5,'TRX','9','GIB',11000,1,'pcs',0,'Rp. ','2022-06-29 22:29:43','2022-06-29 22:29:43'),(6,'TRX','9','SOL',18000,1,'pcs',0,'Rp. ','2022-06-29 22:29:43','2022-06-29 22:29:43'),(7,'TRX','10','SOK',13500,1,'pcs',0,'Rp. ','2022-06-29 22:30:16','2022-06-29 22:30:16'),(8,'TRX','10','GIB',11000,1,'pcs',0,'Rp. ','2022-06-29 22:30:16','2022-06-29 22:30:16'),(9,'TRX','10','SOL',18000,1,'pcs',0,'Rp. ','2022-06-29 22:30:16','2022-06-29 22:30:16'),(10,'TRX','4','SOK',13500,1,'pcs',0,'Rp. ','2022-06-29 22:30:49','2022-06-29 22:30:49'),(11,'TRX','4','GIB',11000,1,'pcs',0,'Rp. ','2022-06-29 22:30:49','2022-06-29 22:30:49'),(12,'TRX','4','SOL',18000,1,'pcs',0,'Rp. ','2022-06-29 22:30:49','2022-06-29 22:30:49'),(13,'TRX','5','SOK',13500,1,'pcs',0,'Rp. ','2022-06-29 22:38:13','2022-06-29 22:38:13'),(14,'TRX','5','GIB',11000,1,'pcs',0,'Rp. ','2022-06-29 22:38:13','2022-06-29 22:38:13'),(15,'TRX','5','SOL',18000,1,'pcs',0,'Rp. ','2022-06-29 22:38:13','2022-06-29 22:38:13'),(16,'TRX','6','SOK',13500,1,'pcs',0,'Rp. ','2022-06-29 23:10:48','2022-06-29 23:10:48'),(17,'TRX','6','GIB',11000,1,'pcs',0,'Rp. ','2022-06-29 23:10:48','2022-06-29 23:10:48'),(18,'TRX','6','SOL',18000,1,'pcs',0,'Rp. ','2022-06-29 23:10:48','2022-06-29 23:10:48'),(19,'TRX','7','SOK',13500,1,'pcs',0,'Rp. ','2022-06-29 23:13:47','2022-06-29 23:13:47'),(20,'TRX','7','GIB',11000,1,'pcs',0,'Rp. ','2022-06-29 23:13:47','2022-06-29 23:13:47'),(21,'TRX','7','SOL',18000,1,'pcs',0,'Rp. ','2022-06-29 23:13:47','2022-06-29 23:13:47'),(22,'TRX','8','SOK',13500,1,'pcs',0,'Rp. ','2022-06-29 23:38:46','2022-06-29 23:38:46'),(23,'TRX','8','GIB',11000,1,'pcs',0,'Rp. ','2022-06-29 23:38:46','2022-06-29 23:38:46'),(24,'TRX','8','SOL',18000,1,'pcs',0,'Rp. ','2022-06-29 23:38:46','2022-06-29 23:38:46'),(25,'TRX','9','SOK',13500,1,'pcs',0,'Rp. ','2022-06-29 23:38:55','2022-06-29 23:38:55'),(26,'TRX','9','GIB',11000,1,'pcs',0,'Rp. ','2022-06-29 23:38:55','2022-06-29 23:38:55'),(27,'TRX','9','SOL',18000,1,'pcs',0,'Rp. ','2022-06-29 23:38:55','2022-06-29 23:38:55'),(28,'TRX','10','SOK',13500,1,'pcs',0,'Rp. ','2022-06-29 23:39:14','2022-06-29 23:39:14'),(29,'TRX','10','GIB',11000,1,'pcs',0,'Rp. ','2022-06-29 23:39:14','2022-06-29 23:39:14'),(30,'TRX','10','SOL',18000,1,'pcs',0,'Rp. ','2022-06-29 23:39:14','2022-06-29 23:39:14'),(31,'TRX','10','SOK',13500,1,'pcs',0,'Rp. ','2022-06-29 23:39:23','2022-06-29 23:39:23'),(32,'TRX','10','GIB',11000,1,'pcs',0,'Rp. ','2022-06-29 23:39:23','2022-06-29 23:39:23'),(33,'TRX','10','SOL',18000,1,'pcs',0,'Rp. ','2022-06-29 23:39:23','2022-06-29 23:39:23'),(34,'TRX','10','SOK',13500,1,'pcs',0,'Rp. ','2022-06-29 23:39:34','2022-06-29 23:39:34'),(35,'TRX','10','GIB',11000,1,'pcs',0,'Rp. ','2022-06-29 23:39:34','2022-06-29 23:39:34'),(36,'TRX','10','SOL',18000,1,'pcs',0,'Rp. ','2022-06-29 23:39:34','2022-06-29 23:39:34'),(37,'TRX','11','SOK',13500,1,'pcs',0,'Rp. ','2022-06-29 23:41:44','2022-06-29 23:41:44'),(38,'TRX','11','GIB',11000,1,'pcs',0,'Rp. ','2022-06-29 23:41:44','2022-06-29 23:41:44'),(39,'TRX','11','SOL',18000,1,'pcs',0,'Rp. ','2022-06-29 23:41:44','2022-06-29 23:41:44');
/*!40000 ALTER TABLE `transaction_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_headers`
--

DROP TABLE IF EXISTS `transaction_headers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_headers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `document_code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document_number` int(11) NOT NULL,
  `user` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` int(11) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_headers`
--

LOCK TABLES `transaction_headers` WRITE;
/*!40000 ALTER TABLE `transaction_headers` DISABLE KEYS */;
INSERT INTO `transaction_headers` VALUES (11,'TRX',4,'1',42500,'2022-06-30','2022-06-29 22:30:49','2022-06-29 22:30:49'),(12,'TRX',5,'1',42500,'2022-06-30','2022-06-29 22:38:13','2022-06-29 22:38:13'),(13,'TRX',6,'1',42500,'2022-06-30','2022-06-29 23:10:48','2022-06-29 23:10:48'),(14,'TRX',7,'1',42500,'2022-06-30','2022-06-29 23:13:47','2022-06-29 23:13:47'),(15,'TRX',8,'1',42500,'2022-06-30','2022-06-29 23:38:46','2022-06-29 23:38:46'),(16,'TRX',9,'1',42500,'2022-06-30','2022-06-29 23:38:55','2022-06-29 23:38:55'),(17,'TRX',10,'1',42500,'2022-06-30','2022-06-29 23:39:14','2022-06-29 23:39:14'),(18,'TRX',10,'1',42500,'2022-06-30','2022-06-29 23:39:23','2022-06-29 23:39:23'),(19,'TRX',10,'1',42500,'2022-06-30','2022-06-29 23:39:34','2022-06-29 23:39:34'),(20,'TRX',11,'1',42500,'2022-06-30','2022-06-29 23:41:44','2022-06-29 23:41:44');
/*!40000 ALTER TABLE `transaction_headers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'penjualan'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-01  9:40:12
